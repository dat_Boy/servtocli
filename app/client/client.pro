QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle
# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS \
            __x86__ \
            __NT__ \
            __OSVERSION__=4 \
            __WIN32__ \
            _CRT_SECURE_NO_DEPRECATE=1 \

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ../../printSK.cpp \
    client.cpp
INCLUDEPATH += "../../../omniORB-4.3.0/include"

LIBS += -L"../../../omniORB-4.3.0/lib/x86_win32" \
../../../omniORB-4.3.0/lib/x86_win32/omniORB4_rt.lib \
../../../omniORB-4.3.0/lib/x86_win32/omniDynamic4_rt.lib \
../../../omniORB-4.3.0/lib/x86_win32/omnithread_rt.lib

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
