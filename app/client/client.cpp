//Client

#include <../../print.hh>
#include "conio.h"
#ifdef HAVE_STD
#  include <iostream>
   using namespace std;
#else
#  include <iostream.h>
#endif


static void SendToServer(Print_ptr e, unsigned char symb)
{

  e->printserver(symb);

  cout << "Client said: " << symb << " " << endl;
}


int main(int argc, char** argv)
{
  try {
    CORBA::ORB_var orb = CORBA::ORB_init(argc, argv, "omniORB4");

    if (argc != 2) {
      cerr << "usage:  client <object reference>" << endl;
      return 1;
    }

    CORBA::Object_var obj = orb->string_to_object(argv[1]);

    Print_var printref = Print::_narrow(obj);

    if (CORBA::is_nil(printref)) {
      cerr << "Can't narrow reference to type Echo (or it was nil)." << endl;
      return 1;
    }
      unsigned char c;
      while(true)
      {
        c = _getch();
        SendToServer(printref, c);
      }
    orb->destroy();
  }
  catch (CORBA::TRANSIENT&) {
    cerr << "Caught system exception TRANSIENT -- unable to contact the "
         << "server." << endl;
  }
  catch (CORBA::SystemException& ex) {
    cerr << "Caught a CORBA::" << ex._name() << endl;
  }
  catch (CORBA::Exception& ex) {
    cerr << "Caught CORBA::Exception: " << ex._name() << endl;
  }
  return 0;
}
