#include <../../print.hh>

#ifdef HAVE_STD
#  include <iostream>
   using namespace std;
#else
#  include <iostream.h>
#endif


class Print_i : public POA_Print
{
public:
  inline Print_i() {}
  virtual ~Print_i() {}
  virtual void printserver(unsigned char mesg);
};


void Print_i::printserver(unsigned char mesg)
{
  cout << "From client: " << mesg << endl;

}


int main(int argc, char** argv)
{
  try {
    CORBA::ORB_var          orb = CORBA::ORB_init(argc, argv);
    CORBA::Object_var       obj = orb->resolve_initial_references("RootPOA");
    PortableServer::POA_var poa = PortableServer::POA::_narrow(obj);

    PortableServer::Servant_var<Print_i> myprint = new Print_i();

    PortableServer::ObjectId_var myprintid = poa->activate_object(myprint);

    // Obtain a reference to the object, and print it out as a
    // stringified IOR.
    obj = myprint->_this();
    CORBA::String_var sior(orb->object_to_string(obj));
    cout << sior << endl;

    PortableServer::POAManager_var pman = poa->the_POAManager();
    pman->activate();

    // Block until the ORB is shut down.
    orb->run();
  }
  catch (CORBA::SystemException& ex) {
    cerr << "Caught CORBA::" << ex._name() << endl;
  }
  catch (CORBA::Exception& ex) {
    cerr << "Caught CORBA::Exception: " << ex._name() << endl;
  }
  return 0;
}
